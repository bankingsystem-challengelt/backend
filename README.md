# backend
Consideraciones que se tomaron:
- Arquitectura Hexagonal.
- Se utilizo el Patron factory method, para la creación de los distintos tipos de cuentas.
- La bd usada es mongo, la misma ubicada en un container de docker.
- La aplicación se levanta con docker-compose

Correr la aplicación:
- mvn clean install
- docker-compose build
- docker-compose up
- IMPORTANTE: ya que la bd esta en un contenedor, ejecutar el siguiente endpoint para inicializar los datos necesarios para el funcionamiento de la app:
- [POST] http://localhost:8080/v1/bank/create
  - {
    "bankId": "1001",
    "bankName": "Banco",
    "commissions": [
    {
    "interestRate": 0.0,
    "overdraftLimit": 200000,
    "type": "checking"
    },
    {
    "interestRate": 0.0,
    "overdraftLimit": 200000,
    "type": "current"
    },
    {
    "interestRate": 0.006,
    "overdraftLimit": 0,
    "type": "saving"
    }
    ]
    }

- Los endpoints realizados se comparten en una colección postman.