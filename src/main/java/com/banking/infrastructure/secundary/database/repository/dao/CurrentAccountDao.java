package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.CurrentAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CurrentAccountDao extends MongoRepository<CurrentAccount,Integer>{
        CurrentAccount findByAccountNumber(String accountNumber);
}
