package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.BankAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BankAccountDao extends MongoRepository<BankAccount,Integer> {
    BankAccount findByAccountNumber(String accountNumber);
}
