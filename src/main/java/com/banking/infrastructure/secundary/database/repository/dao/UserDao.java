package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserDao extends MongoRepository<User,Integer> {
    Optional<User> findByDocumentId(String documentId);
}
