package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.SavingsAccount;
import com.banking.core.domain.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TransactionDao extends MongoRepository<Transaction,Integer> {
    List<Transaction> findByAccountNumber(String accountNumber);
}
