package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.SavingsAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SavingsAccountDao extends MongoRepository<SavingsAccount,Integer> {
    SavingsAccount findByAccountNumber(String accountNumber);
}

