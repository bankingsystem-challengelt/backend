package com.banking.infrastructure.secundary.database.repository;

import com.banking.core.domain.BankCommissions;
import com.banking.core.secundary.BankCommissionsRepository;
import com.banking.infrastructure.secundary.database.repository.dao.BankCommissionDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class BankCommissionsMongoRepository implements BankCommissionsRepository {
    private final BankCommissionDao bankCommissionDao;
    @Override
    public void createBankCommission(List<BankCommissions> bankCommissionsList, String bankId) {
        bankCommissionsList.forEach(bankCommissions -> bankCommissions.setBankId(bankId));
        bankCommissionDao.saveAll(bankCommissionsList);
    }

    @Override
    public BankCommissions findByBankIdAndType(String bankId, String type) {
        return bankCommissionDao.findByBankIdAndType(bankId,type);
    }

    @Override
    public BankCommissions findById(String commissionId) {
        return bankCommissionDao.findById(commissionId).orElseThrow(() -> new IllegalArgumentException("No existe commision para la cuenta"));
    }
}
