package com.banking.infrastructure.secundary.database.repository;

import com.banking.core.domain.Bank;
import com.banking.core.domain.BankCommissions;
import com.banking.core.secundary.BankCommissionsRepository;
import com.banking.core.secundary.BankRepository;
import com.banking.infrastructure.secundary.database.repository.dao.BankDao;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class BankMongoRepository implements BankRepository {
    private final BankDao bankDao;
    private final BankCommissionsRepository bankCommissionsRepository;
    @Value("${props.bankId}")
    String bankId;

    @Override
    public void createBank(Bank bankRequest) {
        bankRequest.setSequence(1);
        bankCommissionsRepository.createBankCommission(bankRequest.getCommissions(),bankId);
        bankDao.save(bankRequest);
    }

    @Override
    public List<Bank> findAll() {
        return bankDao.findAll();
    }

    @Override
    public Bank findBankByBankId(String bankId) {
        return bankDao.findBankByBankId(bankId);
    }

    @Override
    public String createNumberAccount(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(bankId);
        Bank bank = findBankByBankId(bankId);
        stringBuilder.append(bank.getSequence());
        bank.setSequence(bank.getSequence()+1);
        bankDao.save(bank);
        return stringBuilder.toString();
    }

    @Override
    public BankCommissions getBankCommissionByAccount(String type){
        return bankCommissionsRepository.findByBankIdAndType(bankId,type);
    }

    @Override
    public BankCommissions getBankCommissionById(String commissionId) {
        return bankCommissionsRepository.findById(commissionId);
    }
}
