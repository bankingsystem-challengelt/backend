package com.banking.infrastructure.secundary.database.repository;

import com.banking.core.domain.Transaction;
import com.banking.core.secundary.TransactionRepository;
import com.banking.infrastructure.secundary.database.repository.dao.TransactionDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TransactionMongoRepository implements TransactionRepository {
    private final TransactionDao transactionDao;

    @Override
    public List<Transaction> findAll() {
        return transactionDao.findAll();
    }

    @Override
    public List<Transaction> findByAccountNumber(String accountNumber) {
        return transactionDao.findByAccountNumber(accountNumber);
    }

    @Override
    public void save(Transaction transaction) {
        transactionDao.save(transaction);
    }
}
