package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.CheckingAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CheckingAccountDao extends MongoRepository<CheckingAccount,Integer> {
    CheckingAccount findByAccountNumber(String accountNumber);
    CheckingAccount findByAccountHolderId(String accountHolder);
}