package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.Bank;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BankDao extends MongoRepository<Bank,Integer> {
    Bank findBankByBankId(String bankId);
}