package com.banking.infrastructure.secundary.database.repository;

import com.banking.core.domain.User;
import com.banking.core.errors.UserAlreadyExistsException;
import com.banking.core.errors.UserNotExistsException;
import com.banking.core.secundary.UserRepository;
import com.banking.infrastructure.secundary.database.repository.dao.UserDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class UserMongoRepository implements UserRepository {
    private final UserDao userDao;

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public User findByDocumentId(String documentId) {
        return userDao.findByDocumentId(documentId).orElseThrow(() -> new UserNotExistsException(documentId));
    }

    @Override
    public void createUser(User user) {
        if(!userExist(user.getDocumentId())){
            userDao.save(user);
        }else{
            throw new UserAlreadyExistsException(user.getFirstName(), user.getLastName(), user.getDocumentId());
        }
    }

    public boolean userExist(String documentId){
        return userDao.findByDocumentId(documentId).isPresent();
    }
}
