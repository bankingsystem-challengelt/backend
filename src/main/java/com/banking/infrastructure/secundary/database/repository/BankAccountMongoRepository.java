package com.banking.infrastructure.secundary.database.repository;

import com.banking.core.domain.*;
import com.banking.core.domain.factory.BankAccountFactory;
import com.banking.core.errors.IncorrectAccountNumber;
import com.banking.core.secundary.BankAccountRepository;
import com.banking.core.secundary.BankRepository;
import com.banking.core.secundary.TransactionRepository;
import com.banking.infrastructure.request.BankAccountRequest;
import com.banking.infrastructure.request.TransactionRequest;
import com.banking.infrastructure.request.TransferRequest;
import com.banking.infrastructure.secundary.database.repository.dao.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class BankAccountMongoRepository implements BankAccountRepository {
    private final BankAccountDao bankAccountDao;
    private final CheckingAccountDao checkingAccountDao;
    private final SavingsAccountDao savingsAccountDao;

    private final BankRepository bankRepository;
    private final TransactionRepository transactionRepository;

    private final BankAccountFactory bankAccountFactory;


    @Override
    public List<BankAccount> findAll() {
        return bankAccountDao.findAll();
    }

    @Override
    public BankAccount findByAccountNumber(String accountNumber) {
        return bankAccountDao.findByAccountNumber(accountNumber);
    }

    @Override
    public void createAccount(BankAccountRequest account) {
        account.setAccountNumber(createAccountNumber());
        BankAccount bankAccount = bankAccountFactory.createAccount(account);
        BankCommissions bankCommissions = bankRepository.getBankCommissionByAccount(account.getType());
        bankAccount.setCommissionId(bankCommissions.getId());
        bankAccountDao.save(bankAccount);
    }

    private String createAccountNumber(){
        return bankRepository.createNumberAccount();
    }

    public BankAccount deposit(TransactionRequest transactionRequest){
        BankAccount bankAccount = findByAccountNumber(transactionRequest.getAccountNumber());
        if(bankAccount!=null){
            bankAccount.deposit(transactionRequest.getAmount());
            saveTransaction(bankAccount, transactionRequest.getAmount(), "deposit");
            return bankAccount;
        }else{
            throw new IncorrectAccountNumber(transactionRequest.getAccountNumber());
        }
    }

    @Override
    public BankAccount withdraw(TransactionRequest transactionRequest) {
        BankAccount bankAccount = findByAccountNumber(transactionRequest.getAccountNumber());
        if(bankAccount!=null){
            BankCommissions bankCommissions = bankRepository.getBankCommissionById(bankAccount.getCommissionId());
            bankAccount.withdraw(transactionRequest.getAmount(), bankCommissions.getOverdraftLimit());
            saveTransaction(bankAccount, transactionRequest.getAmount(), "withdraw");
            return bankAccount;
        }else{
            throw new IncorrectAccountNumber(transactionRequest.getAccountNumber());
        }
    }

    @Override
    public BankAccount withdrawCheck(TransactionRequest transactionRequest) {
        CheckingAccount checkingAccount = checkingAccountDao.findByAccountNumber(transactionRequest.getAccountNumber());
        if(checkingAccount!=null && checkingAccount.getCheckBook()!=null){
            checkingAccount.cashCheck(transactionRequest.getCheckId(), transactionRequest.getAmount());
            saveTransaction(checkingAccount, transactionRequest.getAmount(), "withdrawCheck");
            return checkingAccount;
        }else{
            throw new IncorrectAccountNumber(transactionRequest.getAccountNumber());
        }
    }

    @Override
    public List<BankAccount> transfer(TransferRequest transferRequest) {
        TransactionRequest withdrawAccount = TransactionRequest.builder()
                .accountNumber(transferRequest.getFromAccountNumber()).amount(transferRequest.getAmount()).build();
        TransactionRequest depositAccount = TransactionRequest.builder()
                .accountNumber(transferRequest.getToAccountNumber()).amount(transferRequest.getAmount()).build();
        BankAccount accountFrom = withdraw(withdrawAccount);
        BankAccount accountTo = deposit(depositAccount);
        return List.of(accountFrom, accountTo);
    }

    @Override
    public BankAccount addInterest(TransactionRequest transactionRequest) {
        SavingsAccount bankAccount = savingsAccountDao.findByAccountNumber(transactionRequest.getAccountNumber());
        if(bankAccount!=null){
            BankCommissions bankCommissions = bankRepository.getBankCommissionById(bankAccount.getCommissionId());
            if(bankCommissions.getInterestRate() > 0.0){
                bankAccount.applyInterest(bankCommissions.getInterestRate());
                saveTransaction(bankAccount, transactionRequest.getAmount(), "addInterest");
            }
            return bankAccount;
        }else{
            throw new IncorrectAccountNumber(transactionRequest.getAccountNumber());
        }
    }

    @Override
    public CheckBook newCheckBook(TransactionRequest transactionRequest) {
        CheckingAccount checkingAccount = checkingAccountDao.findByAccountNumber(transactionRequest.getAccountNumber());
        if(checkingAccount!=null && checkingAccount.getCheckBook()!=null){
            checkingAccount.getCheckBook().newCheckBook();
            saveTransaction(checkingAccount, transactionRequest.getAmount(), "newCheckBook");
            return checkingAccount.getCheckBook();
        }else{
            throw new IncorrectAccountNumber(transactionRequest.getAccountNumber());
        }
    }

    private void saveTransaction(BankAccount bankAccount, double transactionAmount, String type){
        bankAccountDao.save(bankAccount);
        Transaction transaction = Transaction.builder()
                .accountNumber(bankAccount.getAccountNumber())
                .actual_amount(bankAccount.getBalance())
                .amount(transactionAmount)
                .transactionDate(LocalDateTime.now())
                .type(type)
                .build();
        transactionRepository.save(transaction);
    }
}
