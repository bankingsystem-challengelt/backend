package com.banking.infrastructure.secundary.database.repository.dao;

import com.banking.core.domain.Bank;
import com.banking.core.domain.BankCommissions;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BankCommissionDao extends MongoRepository<BankCommissions,String> {
    BankCommissions findByBankIdAndType(String bankId, String type);
}
