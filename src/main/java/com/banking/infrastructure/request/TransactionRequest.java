package com.banking.infrastructure.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransactionRequest {
    private String accountNumber;
    private double amount;
    private long checkId;
}
