package com.banking.infrastructure.request;

import lombok.Data;

@Data
public class TransferRequest {
    private String toAccountNumber;
    private String fromAccountNumber;
    private double amount;
}
