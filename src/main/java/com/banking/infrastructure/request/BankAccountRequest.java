package com.banking.infrastructure.request;

import lombok.Data;

@Data
public class BankAccountRequest {
    private String userDocument;
    private String accountNumber;
    private String type;
    private double balance;
}
