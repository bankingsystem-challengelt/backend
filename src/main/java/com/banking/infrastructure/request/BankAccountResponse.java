package com.banking.infrastructure.request;

import lombok.Data;

@Data
public class BankAccountResponse {
    private String accountNumber;
    private double actualBalance;
}
