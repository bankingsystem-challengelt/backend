package com.banking.infrastructure.primary.controller;

import com.banking.core.domain.BankAccount;
import com.banking.core.services.bankAccount.BankAccountService;
import com.banking.infrastructure.request.BankAccountRequest;
import com.banking.infrastructure.request.TransactionRequest;
import com.banking.infrastructure.request.TransferRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/account")
public class BankAccountController {

    private final BankAccountService bankAccountService;

    @GetMapping("/all")
    public List<BankAccount> listAll(){
        return bankAccountService.findAll();
    }

    @GetMapping("/by-account-number/{accountNumber}")
    public ResponseEntity<?> findByAccountNumber(@PathVariable("accountNumber") String accountNumber) {
        try{
            return ResponseEntity.ok(bankAccountService.findByAccountNumber(accountNumber));
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }

    @PostMapping("/create")
    public ResponseEntity<?> createAccount(@RequestBody() BankAccountRequest account) {
        try{
            bankAccountService.createAccount(account);
            return ResponseEntity.status(201).build();
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }

    @PutMapping("/deposit")
    public ResponseEntity<?> deposit(@RequestBody TransactionRequest transactionRequest){
        try{
            return ResponseEntity.ok(bankAccountService.deposit(transactionRequest));
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }

    @PutMapping("/withdraw")
    public ResponseEntity<?> withdraw(@RequestBody TransactionRequest transactionRequest){
        try{
            return ResponseEntity.ok(bankAccountService.withdraw(transactionRequest));
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }

    @PutMapping("/withdraw/check")
    public ResponseEntity<?> withdrawCheck(@RequestBody TransactionRequest transactionRequest){
        try{
            return ResponseEntity.ok(bankAccountService.withdrawCheck(transactionRequest));
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }

    @PutMapping("/transfer")
    public ResponseEntity<?> transfer(@RequestBody TransferRequest transferRequest){
        try{
            return ResponseEntity.ok(bankAccountService.transfer(transferRequest));
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }


    @PutMapping("/add-interest")
    public ResponseEntity<?> addInterest(@RequestBody TransactionRequest transactionRequest){
        try{
            return ResponseEntity.ok(bankAccountService.addInterest(transactionRequest));
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }


    @PutMapping("/new-checkBook")
    public ResponseEntity<?> newCheckBook(@RequestBody  TransactionRequest transactionRequest){
        try{
            return ResponseEntity.ok(bankAccountService.newCheckBook(transactionRequest));
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }
}
