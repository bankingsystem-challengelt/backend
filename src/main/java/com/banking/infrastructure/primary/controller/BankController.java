package com.banking.infrastructure.primary.controller;

import com.banking.core.domain.Bank;
import com.banking.core.services.bank.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/bank")
public class BankController {
    private final BankService bankService;

    @PostMapping("/create")
    public ResponseEntity<?> createBank(@RequestBody() Bank bankRequest){
        try{
            bankService.createBank(bankRequest);
            return ResponseEntity.status(201).build();
        }catch (
        HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> listAll(){
        try{
            return ResponseEntity.ok(bankService.findAll());
        }catch (HttpStatusCodeException ex) {
            return ResponseEntity.status(ex.getStatusCode()).contentType(MediaType.APPLICATION_JSON).body(ex.getResponseBodyAsString());
        }catch (Exception exception){
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
        }
    }
}
