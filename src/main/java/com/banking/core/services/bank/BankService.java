package com.banking.core.services.bank;

import com.banking.core.domain.Bank;
import com.banking.core.domain.BankAccount;

import java.util.List;

public interface BankService {
    void createBank(Bank bankRequest);

    List<Bank> findAll();
}
