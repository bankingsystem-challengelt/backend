package com.banking.core.services.bank.impl;

import com.banking.core.domain.Bank;
import com.banking.core.domain.BankAccount;
import com.banking.core.secundary.BankRepository;
import com.banking.core.services.bank.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BankServiceImpl implements BankService {
    private final BankRepository bankRepository;

    @Override
    public void createBank(Bank bankRequest) {
        bankRepository.createBank(bankRequest);
    }

    @Override
    public List<Bank> findAll() {
        return bankRepository.findAll();
    }
}
