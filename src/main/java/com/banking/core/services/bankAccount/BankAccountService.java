package com.banking.core.services.bankAccount;

import com.banking.core.domain.BankAccount;
import com.banking.core.domain.CheckBook;
import com.banking.infrastructure.request.BankAccountRequest;
import com.banking.infrastructure.request.BankAccountResponse;
import com.banking.infrastructure.request.TransactionRequest;
import com.banking.infrastructure.request.TransferRequest;

import java.util.List;

public interface BankAccountService {
    List<BankAccount> findAll();
    BankAccount findByAccountNumber(String accountNumber);
    void createAccount(BankAccountRequest account);
    BankAccountResponse deposit(TransactionRequest transactionRequest);
    BankAccountResponse withdraw(TransactionRequest transactionRequest);
    BankAccountResponse withdrawCheck(TransactionRequest transactionRequest);
    List<BankAccountResponse> transfer(TransferRequest transferRequest);
    CheckBook newCheckBook(TransactionRequest transactionRequest);
    BankAccountResponse addInterest(TransactionRequest transactionRequest);
}
