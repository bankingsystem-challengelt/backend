package com.banking.core.services.bankAccount.impl;

import com.banking.core.domain.BankAccount;
import com.banking.core.domain.CheckBook;
import com.banking.core.errors.UserNotExistsException;
import com.banking.core.secundary.BankAccountRepository;
import com.banking.core.secundary.UserRepository;
import com.banking.core.services.bankAccount.BankAccountService;
import com.banking.infrastructure.request.BankAccountRequest;
import com.banking.infrastructure.request.BankAccountResponse;
import com.banking.infrastructure.request.TransactionRequest;
import com.banking.infrastructure.request.TransferRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountRepository bankAccountRepository;
    private final UserRepository userRepository;

    @Override
    public List<BankAccount> findAll() {
        return bankAccountRepository.findAll();
    }

    @Override
    public BankAccount findByAccountNumber(String accountNumber) {
        return bankAccountRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public void createAccount(BankAccountRequest account) {
        if(userRepository.userExist(account.getUserDocument())){
            bankAccountRepository.createAccount(account);
        }else{
            throw new UserNotExistsException(account.getUserDocument());
        }
    }

    @Override
    public BankAccountResponse deposit(TransactionRequest transactionRequest) {
        BankAccount bankAccount = bankAccountRepository.deposit(transactionRequest);
        BankAccountResponse bankAccountResponse = new BankAccountResponse();
        bankAccountResponse.setAccountNumber(bankAccount.getAccountNumber());
        bankAccountResponse.setActualBalance(bankAccount.getBalance());
        return bankAccountResponse;
    }

    @Override
    public BankAccountResponse withdraw(TransactionRequest transactionRequest) {
        BankAccount bankAccount = bankAccountRepository.withdraw(transactionRequest);
        BankAccountResponse bankAccountResponse = new BankAccountResponse();
        bankAccountResponse.setAccountNumber(bankAccount.getAccountNumber());
        bankAccountResponse.setActualBalance(bankAccount.getBalance());
        return bankAccountResponse;
    }

    @Override
    public BankAccountResponse withdrawCheck(TransactionRequest transactionRequest) {
        BankAccount bankAccount = bankAccountRepository.withdrawCheck(transactionRequest);
        BankAccountResponse bankAccountResponse = new BankAccountResponse();
        bankAccountResponse.setAccountNumber(bankAccount.getAccountNumber());
        bankAccountResponse.setActualBalance(bankAccount.getBalance());
        return bankAccountResponse;
    }

    @Override
    public List<BankAccountResponse> transfer(TransferRequest transferRequest) {
        List<BankAccount> bankAccounts = bankAccountRepository.transfer(transferRequest);
        List<BankAccountResponse> bankAccountResponses = new ArrayList<>();
        bankAccounts.forEach(bankAccount -> {
            BankAccountResponse bankAccountResponse = new BankAccountResponse();
            bankAccountResponse.setAccountNumber(bankAccount.getAccountNumber());
            bankAccountResponse.setActualBalance(bankAccount.getBalance());
            bankAccountResponses.add(bankAccountResponse);
        });

        return bankAccountResponses;
    }

    @Override
    public BankAccountResponse addInterest(TransactionRequest transactionRequest) {
        BankAccount bankAccount = bankAccountRepository.addInterest(transactionRequest);
        BankAccountResponse bankAccountResponse = new BankAccountResponse();
        bankAccountResponse.setAccountNumber(bankAccount.getAccountNumber());
        bankAccountResponse.setActualBalance(bankAccount.getBalance());
        return bankAccountResponse;
    }

    @Override
    public CheckBook newCheckBook(TransactionRequest transactionRequest) {
        return bankAccountRepository.newCheckBook(transactionRequest);
    }


}
