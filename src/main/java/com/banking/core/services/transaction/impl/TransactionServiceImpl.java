package com.banking.core.services.transaction.impl;

import com.banking.core.domain.Transaction;
import com.banking.core.secundary.TransactionRepository;
import com.banking.core.services.transaction.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionrepository;

    @Override
    public List<Transaction> findAll() {
        return transactionrepository.findAll();
    }

    @Override
    public List<Transaction> findByAccountNumber(String accountNumber) {
        return transactionrepository.findByAccountNumber(accountNumber);
    }
}
