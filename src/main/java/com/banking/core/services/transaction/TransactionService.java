package com.banking.core.services.transaction;

import com.banking.core.domain.Transaction;

import java.util.List;

public interface TransactionService {
    List<Transaction> findAll();
    List<Transaction> findByAccountNumber(String accountNumber);
}
