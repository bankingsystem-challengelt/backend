package com.banking.core.services.user.impl;

import com.banking.core.domain.User;
import com.banking.core.secundary.UserRepository;
import com.banking.core.services.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findByDocumentId(String documentId) {
        return userRepository.findByDocumentId(documentId);
    }

    @Override
    public void createUser(User user) {
        userRepository.createUser(user);
    }
}
