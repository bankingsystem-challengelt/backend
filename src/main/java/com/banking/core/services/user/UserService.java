package com.banking.core.services.user;

import com.banking.core.domain.User;

import java.util.List;

public interface UserService{

    List<User> findAll();

    User findByDocumentId(String documentId);

    void createUser(User user);
}
