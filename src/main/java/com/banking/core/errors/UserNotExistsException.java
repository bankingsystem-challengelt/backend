package com.banking.core.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;

public class UserNotExistsException extends HttpClientErrorException {

    public UserNotExistsException(String documentId) {
        super(HttpStatus.BAD_REQUEST,"404",
                String.format("El usuario con documento %s no existe en el sistema", documentId).getBytes(StandardCharsets.UTF_8)
                ,null);
    }
}
