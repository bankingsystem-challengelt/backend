package com.banking.core.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;

public class IncorrectAccountNumber extends HttpClientErrorException {
    public IncorrectAccountNumber(String accountNumber){
        super(HttpStatus.BAD_REQUEST,"404",
                String.format("El numero de cuenta no es correcto: %s", accountNumber).getBytes(StandardCharsets.UTF_8)
                ,null);
    }
}
