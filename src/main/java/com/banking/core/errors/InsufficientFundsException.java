package com.banking.core.errors;

import org.springframework.web.client.HttpClientErrorException;

public class InsufficientFundsException extends RuntimeException {
    public InsufficientFundsException() {
        super("No hay fondos suficientes para completar la transaccion");
    }

}
