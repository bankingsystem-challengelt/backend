package com.banking.core.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;

public class IncorrectAccountType extends HttpClientErrorException {
    public IncorrectAccountType(String type){
        super(HttpStatus.BAD_REQUEST,"404",
                String.format("El tipo de cuenta no es correcto: %s", type).getBytes(StandardCharsets.UTF_8)
                ,null);
    }
}
