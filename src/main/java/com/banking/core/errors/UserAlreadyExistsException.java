package com.banking.core.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;

public class UserAlreadyExistsException extends HttpClientErrorException {

    public UserAlreadyExistsException(String firstname, String lastName, String documentId) {
        super(HttpStatus.BAD_REQUEST,"404",
                String.format("El usuario %s %s con documento: %s. Ya existe en el sistema", firstname, lastName, documentId).getBytes(StandardCharsets.UTF_8)
                ,null);
    }
}
