package com.banking.core.secundary;

import com.banking.core.domain.Bank;
import com.banking.core.domain.BankCommissions;

import java.util.List;

public interface BankRepository {
    void createBank(Bank bankRequest);
    List<Bank> findAll();
    Bank findBankByBankId(String bankId);
    String createNumberAccount();
    BankCommissions getBankCommissionByAccount(String type);
    BankCommissions getBankCommissionById(String commissionId);
}
