package com.banking.core.secundary;

import com.banking.core.domain.BankAccount;
import com.banking.core.domain.CheckBook;
import com.banking.infrastructure.request.BankAccountRequest;
import com.banking.infrastructure.request.TransactionRequest;
import com.banking.infrastructure.request.TransferRequest;

import java.util.List;

public interface BankAccountRepository {
    List<BankAccount> findAll();
    BankAccount findByAccountNumber(String accountNumber);
    void createAccount(BankAccountRequest account);
    BankAccount deposit(TransactionRequest transactionRequest);
    BankAccount withdraw(TransactionRequest transactionRequest);
    BankAccount withdrawCheck(TransactionRequest transactionRequest);
    List<BankAccount> transfer(TransferRequest transferRequest);
    BankAccount addInterest(TransactionRequest transactionRequest);
    CheckBook newCheckBook(TransactionRequest transactionRequest);
}
