package com.banking.core.secundary;

import com.banking.core.domain.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();
    boolean userExist(String documentId);
    User findByDocumentId(String documentId);
    void createUser(User user);

}
