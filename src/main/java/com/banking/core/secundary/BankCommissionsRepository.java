package com.banking.core.secundary;

import com.banking.core.domain.BankCommissions;

import java.util.List;

public interface BankCommissionsRepository {
    void createBankCommission(List<BankCommissions> bankCommissionsList, String bankId);
    BankCommissions findByBankIdAndType(String bankId, String type);
    BankCommissions findById(String commissionId);
}
