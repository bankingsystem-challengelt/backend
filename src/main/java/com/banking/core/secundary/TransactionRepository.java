package com.banking.core.secundary;

import com.banking.core.domain.Transaction;

import java.util.List;

public interface TransactionRepository {
    List<Transaction> findAll();
    List<Transaction> findByAccountNumber(String accountNumber);
    void save(Transaction transaction);
}
