package com.banking.core.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "bank-commissions")
public class BankCommissions {
    @Id
    private String id;
    private String bankId;
    private double interestRate;
    private double overdraftLimit;
    private String type;
}
