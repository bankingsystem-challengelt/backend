package com.banking.core.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

@EqualsAndHashCode(callSuper = true)
@Document(collection = "bankAccounts")
@Data
public class CurrentAccount extends BankAccount{

    public CurrentAccount(String accountHolder, String accountNumber) {
        super(accountHolder, accountNumber);
    }

    public CurrentAccount() {
        super();
    }
}
