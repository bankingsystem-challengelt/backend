package com.banking.core.domain.factory;

import com.banking.core.domain.BankAccount;
import com.banking.infrastructure.request.BankAccountRequest;
import org.springframework.stereotype.Component;

@Component
public class BankAccountFactoryImpl extends BankAccountFactory{
    @Override
    public BankAccount createAccount(BankAccountRequest bankAccountRequest) {
        return Factory.createBankAccount(bankAccountRequest);
    }
}
