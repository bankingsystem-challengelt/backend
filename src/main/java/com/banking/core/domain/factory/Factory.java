package com.banking.core.domain.factory;

import com.banking.core.domain.*;
import com.banking.core.errors.IncorrectAccountType;
import com.banking.infrastructure.request.BankAccountRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class Factory {
    private static final Map<String, Supplier<BankAccount>> factory = new HashMap<>();

    static{
        factory.put("checking", CheckingAccount::new);
        factory.put("current", CurrentAccount::new);
        factory.put("saving", SavingsAccount::new);
    }

    public static BankAccount createBankAccount(BankAccountRequest account){
        Supplier<BankAccount> bankAccount = factory.get(account.getType().toLowerCase());
        if(bankAccount!=null){
            BankAccount resultBankAccount = bankAccount.get();
            resultBankAccount.completeAccount(account);
            return resultBankAccount;
        }
        throw new IncorrectAccountType(account.getType());
    }
}
