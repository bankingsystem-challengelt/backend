package com.banking.core.domain.factory;

import com.banking.core.domain.BankAccount;
import com.banking.infrastructure.request.BankAccountRequest;

public abstract class BankAccountFactory {
    public abstract BankAccount createAccount(BankAccountRequest bankAccountRequest);
}
