package com.banking.core.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@Document(collection = "bank")
public class Bank {
    @Id
    private String id;
    @LastModifiedDate
    private OffsetDateTime lastUpdated;
    private String bankId;
    private String bankName;
    private List<BankCommissions> commissions;
    private long sequence;
}
