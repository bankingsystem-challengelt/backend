package com.banking.core.domain;

import com.banking.infrastructure.request.BankAccountRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@EqualsAndHashCode(callSuper = true)
@Document(collection = "bankAccounts")
@Data
public class CheckingAccount extends CurrentAccount{
    @Field("checkBook")
    private CheckBook checkBook;

    public CheckingAccount() {
        super();
    }

    public void setCheckBook(String accountNumber) {
        this.checkBook = new CheckBook(accountNumber);
    }

    public void cashCheck(long checkNumber, double amount){
        this.checkBook.validateCheck(checkNumber);
        withdraw(amount);
        this.checkBook.cashedCheck(checkNumber);
    }

    @Override
    public void completeAccount(BankAccountRequest account) {
        setAccountHolderId(account.getUserDocument());
        setBalance(account.getBalance());
        setAccountNumber(account.getAccountNumber());
        setType(account.getType());
        setCheckBook(account.getAccountNumber());
    }
}
