package com.banking.core.domain;

import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

@Data
public class CheckBook {
    private String accountNumber;
    private Integer checkBookNumber;
    private Integer checkNumberFrom;
    private Integer checkNumberTo;
    private List<Long> checks;
    private boolean checkbookSoldOut;

    public CheckBook(String accountNumber) {
        this.accountNumber = accountNumber;
        this.checkBookNumber = 1;
        this.checkNumberFrom = 1;
        this.checkNumberTo = 25;
        this.checkbookSoldOut = false;
        initCheckBook();
    }

    public void initCheckBook() {
        checks = IntStream.rangeClosed(checkNumberFrom, checkNumberTo)
                .mapToObj(id -> Long.parseLong(accountNumber + checkBookNumber + String.format("%02d",id)))
                .collect(Collectors.toList());
    }

    public void newCheckBook() {
        this.checkBookNumber++;
        initCheckBook();
    }

    public boolean checkbookSoldOut() {
        return this.checks.isEmpty();
    }

    public void cashedCheck(long checkNumber){
        checks.removeIf(check -> check.equals(checkNumber));
        if (checks.isEmpty()){
            this.checkbookSoldOut = true;
        }
    }

    public void validateCheck(long checkNumber){
        if(checkbookSoldOut()){
            throw new RuntimeException("La chequera ya no tiene cheques disponibles ");
        }

        checks.stream()
                .filter(check -> check.equals(checkNumber))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("El cheque no es valido "+checkNumber));
    }
}
