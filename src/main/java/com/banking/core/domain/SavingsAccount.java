package com.banking.core.domain;

import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

@EqualsAndHashCode(callSuper = true)
@Document(collection = "bankAccounts")
public class SavingsAccount extends BankAccount{

    public SavingsAccount(String accountHolder,String accountNumber) {
        super(accountHolder,accountNumber);
    }

    public SavingsAccount() {
        super();
    }

    public double calculateInterest(double interestRate) {
        return getBalance() * interestRate;
    }

    public void applyInterest(double interestRate) {
        double intereses = getBalance() * interestRate;
        deposit(intereses);
    }
}
