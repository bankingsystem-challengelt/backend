package com.banking.core.domain;

import com.banking.core.errors.InsufficientFundsException;
import com.banking.infrastructure.request.BankAccountRequest;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "bankAccounts")
public class BankAccount implements BankAccountInterface{
    @Id
    private String id;
    private String accountHolderId;
    private String commissionId;
    private String accountNumber;
    private String type;
    private double balance;

    public BankAccount(String accountHolderId, String accountNumber) {
        this.accountNumber = accountNumber;
        this.accountHolderId = accountHolderId;
        this.balance = 0.0;
    }

    public BankAccount() {
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public void withdraw(double amount) {
        if(this.balance > amount){
            this.balance -= amount;
        }else{
            throw new InsufficientFundsException();
        }
    }

    public void withdraw(double amount, double overdraftLimit) {
        if (this.balance + overdraftLimit >= amount) {
            this.balance -= amount;
        }else{
            throw new InsufficientFundsException();
        }
    }

    public void completeAccount(BankAccountRequest account) {
        this.accountHolderId = account.getUserDocument();
        this.balance = account.getBalance();
        this.accountNumber = account.getAccountNumber();
        this.type = account.getType();
    }
}
